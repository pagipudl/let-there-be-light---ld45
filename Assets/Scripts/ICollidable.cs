﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//just for the reason of not activating a collision event on both objects
public interface ICollidable
{
	void Trigger();
}
