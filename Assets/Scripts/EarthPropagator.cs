﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EarthPropagator : MonoBehaviour
{
	[SerializeField]
	protected GameObject linePrefab;

	private void OnTriggerEnter2D(Collider2D collision)
	{
		var planet = collision.GetComponent<PlanetCollision>();
		if (planet != null && !planet.IsEarth)
		{
			var body = collision.GetComponent<OrbitableBody>();
			Instantiate(linePrefab).GetComponent<ColonizeLine>().StartColonize(transform, body);
		}
	}
}
