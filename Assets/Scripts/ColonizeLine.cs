﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColonizeLine : MonoBehaviour
{
	[SerializeField]
	protected LineRenderer line;
	[SerializeField]
	protected GameObject earthPrefab;

	public void StartColonize(Transform earth, OrbitableBody target)
	{
		StartCoroutine(Colonize(earth, target));
	}

	private void Start()
	{
	}

	IEnumerator Colonize(Transform earth, OrbitableBody target)
	{
		var timer = 0f;
		while (timer < 2f)
		{
			timer += Time.deltaTime;
			if (earth == null || target == null)
			{
				Destroy(gameObject);
				yield break;
			}
			line.SetPosition(0, earth.position);
			line.SetPosition(1, Vector3.LerpUnclamped(earth.position, target.transform.position, timer / 2f));
			yield return null;
		}
		var go = Instantiate(earthPrefab, target.transform.position, target.transform.rotation);
		var newBody = go.GetComponent<OrbitableBody>();
		newBody.Mass = target.Mass;
		newBody.Rigid.velocity = target.Rigid.velocity;
		newBody.Rigid.angularVelocity = target.Rigid.angularVelocity;
		target.gameObject.SetActive(false);
		Destroy(target.gameObject);
		Destroy(gameObject);
	}
}
