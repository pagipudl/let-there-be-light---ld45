﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbitableBody : MonoBehaviour
{
	[SerializeField]
	protected float mass;
	public float Mass { get => mass; set { mass = value; rigid.mass = value; } }
	[SerializeField]
	protected Rigidbody2D rigid;
	public Rigidbody2D Rigid => rigid;

	public const double G = 6.693e2; //grav. constant, change it to work with my game size (1000 units horizontaly)

	float MinimalDistance(OrbitableBody other)
	{
		return Mathf.Max(transform.localScale.x, other.transform.localScale.x);
	}

	public Vector2 VectorTo(OrbitableBody other)
	{
		var dir3D = other.transform.position - transform.position;
		return new Vector2(dir3D.x, dir3D.y);
	}

	public float SqrDistance(OrbitableBody other)
	{
		return (VectorTo(other)).sqrMagnitude;
	}

	public float GravityForceFrom(OrbitableBody other)
	{
		return (float)-(G * mass * other.mass) / Mathf.Clamp(SqrDistance(other), MinimalDistance(other), float.MaxValue);
	}

	public float InitialVelocityFrom(OrbitableBody other)
	{
		return -Mathf.Sqrt((float)(G * other.mass) / Mathf.Clamp(Mathf.Sqrt(SqrDistance(other)), MinimalDistance(other), float.MaxValue));
	}

	private void Awake()
	{
		GravitySimulator.AddBody(this);
		rigid.mass = mass;
	}

	private void OnDisable()
	{
		GravitySimulator.RemoveBody(this);
	}
}
