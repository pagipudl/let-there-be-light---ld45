﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StarCollision : MonoBehaviour, ICollidable
{
	[SerializeField]
	GameObject explosionPrefab;

	bool triggered = false;

	public void Trigger()
	{
		triggered = true;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (triggered)
			return;
		var other = collision.gameObject.GetComponent<ICollidable>();
		if (other != null && !(other is BlackholeCollision)) //add my parameters to the other hole and destroy self
		{
			other.Trigger();
			var go = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
			Destroy(go, go.GetComponent<ParticleSystem>().main.duration);
			var body = GetComponent<OrbitableBody>();
			var otherBody = collision.gameObject.GetComponent<OrbitableBody>();
			transform.localScale += otherBody.transform.localScale;
			body.Mass += otherBody.Mass;
			body.Rigid.AddForce(otherBody.Rigid.velocity * otherBody.Mass, ForceMode2D.Impulse);
			Destroy(otherBody.gameObject);
		}
	}
}
