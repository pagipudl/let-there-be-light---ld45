﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(OrbitableBody))]
public class BlackholeCollision : MonoBehaviour, ICollidable
{
	bool triggered = false;
	public void Trigger()
	{
		triggered = true;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (triggered)
			return;
		var other = collision.gameObject.GetComponent<ICollidable>();
		if (other != null) //add my parameters to the other hole and destroy self
		{
			other.Trigger();
			var body = GetComponent<OrbitableBody>();
			var otherBody = collision.gameObject.GetComponent<OrbitableBody>();
			foreach (var system in GetComponentsInChildren<ParticleSystem>())
			{
				var shape = system.shape;
				shape.radius += shape.radius * (otherBody.transform.localScale.x / transform.localScale.x);
				var size = system.main.startSize;
				size.constant += size.constant * (otherBody.transform.localScale.x / transform.localScale.x);
				var rate = system.emission.rateOverTime;
				rate.constant += rate.constant * (otherBody.transform.localScale.x / transform.localScale.x);
			}
			transform.localScale += otherBody.transform.localScale;
			body.Mass += otherBody.Mass;
			body.Rigid.AddForce(otherBody.Rigid.velocity*otherBody.Mass, ForceMode2D.Impulse);
			Destroy(otherBody.gameObject);
		}
	}
}
