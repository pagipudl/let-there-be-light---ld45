﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravitySimulator : MonoBehaviour
{
	static GravitySimulator instance;

	[SerializeField]
	protected Transform cameraFollow;

	List<OrbitableBody> bodies = new List<OrbitableBody>();

	bool started;

	private void Awake()
	{
		if (instance != null)
			Debug.LogError(instance);
		instance = this;
	}

	public static void AddBody(OrbitableBody body)
	{
		instance.bodies.Add(body);
	}

	public static void RemoveBody(OrbitableBody body)
	{
		instance.bodies.Remove(body);
	}

	public static int GetPlanetCount()
	{
		int count = 0;
		foreach (var body in instance.bodies)
		{
			var planet = body.GetComponent<PlanetCollision>();
			if (planet != null)
				count++;
		}
		return count;
	}

	public static void StartSimulation()
	{
		if (instance.started)
			return;
		instance.started = true;
		GameRuler.instance.StartGame();
		Camera.main.GetComponent<Cinemachine.CinemachineBrain>().ActiveVirtualCamera.Follow = instance.cameraFollow;

		instance.bodies.Sort((b1, b2) => b2.Mass.CompareTo(b1.Mass));
		foreach (var body in instance.bodies)
		{
			float sqrdist = float.MaxValue;
			OrbitableBody chosen = null;
			OrbitableBody secondBest = null;
			foreach (var otherBody in instance.bodies)
			{
				if (otherBody == body)
					continue;
				var d = otherBody.SqrDistance(body);
				if (d < sqrdist)
				{
					if (otherBody.Mass <= body.Mass)
					{
						secondBest = otherBody;
						continue;
					}
					sqrdist = d;
					chosen = otherBody;
				}
			}
			if (chosen != null)
				instance.ApplyInitial(body, chosen);
			else if (secondBest != null)
				instance.ApplyInitial(body, secondBest);
		}
	}

	private void FixedUpdate()
	{
		if (!started)
			return;
		Vector3 middlePos = Vector3.zero;
		float totalMass = 0f;
		for (int i = 0; i < bodies.Count; i++)
		{
			var body = bodies[i];
			middlePos += body.transform.position * body.Mass;
			totalMass += body.Mass;
			if (body.transform.position.magnitude > 3000)
			{
				body.Rigid.velocity *= -1;
				body.transform.position = Vector3.ClampMagnitude(body.transform.position, 2990);
				continue;
			}
			for (int j = i + 1; j < bodies.Count; j++)
			{
				var other = bodies[j];
				ApplyGravity(body, other);
			}
			var lookR = Quaternion.LookRotation(body.Rigid.velocity, Vector3.back);
			body.Rigid.MoveRotation(Quaternion.Lerp(body.transform.rotation, lookR, body.Rigid.velocity.sqrMagnitude / body.Mass));
		}
		if (totalMass > 0f)
		{
			middlePos /= totalMass;
			cameraFollow.position = middlePos;
		}
	}

	private void ApplyGravity(OrbitableBody one, OrbitableBody two)
	{
		var force = one.GravityForceFrom(two);
		var dir = one.VectorTo(two).normalized;
		one.Rigid.AddForce(-dir * force, ForceMode2D.Force);
		two.Rigid.AddForce(dir * force, ForceMode2D.Force);
	}

	private void ApplyInitial(OrbitableBody one, OrbitableBody two)
	{
		var forceOne = one.InitialVelocityFrom(two);
		var forceTwo = two.InitialVelocityFrom(one);
		var dir = one.VectorTo(two).normalized;
		var perp = new Vector2(-dir.y, dir.x);
		//if (two.Mass > one.Mass)
		one.Rigid.velocity += -perp * forceOne;
		if (one.Mass < two.Mass)
			one.Rigid.velocity += two.Rigid.velocity;
		//else if (two.Mass < one.Mass)
		//two.Rigid.velocity += perp * forceTwo;

	}
}
