﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetCollision : MonoBehaviour, ICollidable
{
	bool triggered = false;

	[SerializeField]
	GameObject explosionPrefab;
	[SerializeField]
	protected bool isEarth;
	public bool IsEarth => isEarth;

	private void Start()
	{
		if (isEarth)
			GameRuler.instance.AddEarth();
	}

	public void Trigger()
	{
		triggered = true;
	}
	private void OnCollisionEnter2D(Collision2D collision)
	{
		if (triggered)
		{
			return;
		}
		var other = collision.gameObject.GetComponent<ICollidable>();
		if (other != null && (other is PlanetCollision))
		{
			other.Trigger();
			var go = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
			Destroy(go, go.GetComponent<ParticleSystem>().main.duration);
			Destroy(gameObject);
			Destroy(collision.gameObject);
		}
	}

	private void OnDestroy()
	{
		if (isEarth)
			GameRuler.instance.RemoveEarth();
	}
}
