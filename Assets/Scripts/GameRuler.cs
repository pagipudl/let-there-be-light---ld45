﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameRuler : MonoBehaviour
{
	public static GameRuler instance;
	static int level = 0;
	public static int Level => level;
	static bool cheatsOn = false;
	public static bool CheatsOn => cheatsOn;
	static bool gameOverWhenEarthIsDestroyed = false;
	public static bool GameOverWhenEarthIsDestroyed => gameOverWhenEarthIsDestroyed;

	private void Awake()
	{
		if (instance != null)
			Debug.LogError(this);
		instance = this;
	}

	[SerializeField]
	protected TMPro.TMP_Text statusText;
	[SerializeField]
	protected GameObject escMenu;
	[SerializeField]
	protected UnityEngine.UI.Toggle cheatToggle;
	[SerializeField]
	protected UnityEngine.UI.Toggle goverToggle;
	[SerializeField]
	protected UnityEngine.UI.Slider audioSlider;
	[SerializeField]
	AudioClip winAudio;
	[SerializeField]
	AudioClip loseAudio;
	bool gameStarted;
	bool gameOver;
	bool victory;
	int totalPlanets;
	int totalEarths = 0;
	int earthsAlive = 0;
	float timeAtStart;

	private void Start()
	{
		statusText.text = "Level " + level;
		cheatToggle.isOn = cheatsOn;
		goverToggle.isOn = gameOverWhenEarthIsDestroyed;
		audioSlider.value = AudioListener.volume;
	}

	public void StartGame()
	{
		if (!cheatsOn)
			totalPlanets = PrefabAdder.PlanetsInLevel(level) + PrefabAdder.EarthsInLevel(level);
		else
			totalPlanets = GravitySimulator.GetPlanetCount();
		gameStarted = true;
		timeAtStart = Time.time;
	}

	public void AddEarth()
	{
		totalEarths++;
		earthsAlive++;
	}

	public void RemoveEarth()
	{
		//totalEarths--; //dont need this for win conditions
		earthsAlive--;
		if (gameOverWhenEarthIsDestroyed)
			GameOver();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape))
			ToggleEscMenu();
		if (!gameStarted)
		{
			if (victory)
			{
				if (Input.GetKeyDown(KeyCode.Space))
				{
					level++;
					UnityEngine.SceneManagement.SceneManager.LoadScene(1);
				}
			}
			if (gameOver)
			{
				if (Input.GetKeyDown(KeyCode.Space))
				{
					UnityEngine.SceneManagement.SceneManager.LoadScene(1);
				}
			}
			return;
		}
		var timeRemaining = (timeAtStart + 30 - Time.time);
		var percentColonized = 100f * totalEarths / (float)totalPlanets;
		if (percentColonized >= 75)
		{
			GetComponent<AudioSource>().PlayOneShot(winAudio);
			statusText.text = "Victory! Press space to continue.";
			gameStarted = false;
			victory = true;
		}
		else if (totalPlanets - totalEarths == 0 || timeRemaining < 0 || earthsAlive < 1 || Input.GetKeyDown(KeyCode.Space))
		{
			GameOver();
		}
		else
		{

			statusText.text = (int)percentColonized + "% of planets colonized. " + (int)timeRemaining + " seconds remaining.";
		}
	}

	void GameOver()
	{
		if (victory)
			return;
		GetComponent<AudioSource>().PlayOneShot(loseAudio);
		statusText.text = "Game over :( Press space to retry.";
		gameStarted = false;
		gameOver = true;
	}

	void ToggleEscMenu()
	{
		escMenu.SetActive(!escMenu.activeSelf);
		if (escMenu.activeSelf)
		{
			Time.timeScale = 0f;
		}
		else
		{
			Time.timeScale = 1f;
		}
	}

	public void EnableCheats(bool cheats)
	{
		cheatsOn = cheats;
	}

	public void EnableGameoverEarth(bool gover)
	{
		gameOverWhenEarthIsDestroyed = gover;
	}

	public void Quit()
	{
		Application.Quit();
	}

	public void SetVolume()
	{
		AudioListener.volume = audioSlider.value;
	}
}
