﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrefabAdder : MonoBehaviour
{
	[System.Serializable]
	public struct BodyAndCount
	{
		public GameObject prefab;
		public int count;
		public AudioClip audio;
	}
	[SerializeField]
	TMPro.TMP_Text infoText;
	[SerializeField]
	AudioSource bgAudio;
	[SerializeField]
	AudioClip mainMusic;
	[SerializeField]
	protected BodyAndCount[] bodies;

	int bodyIndex = 0;

	private void Start()
	{
		bodies[0].count = BlackholesInLevel(GameRuler.Level);
		bodies[1].count = StarsInLevel(GameRuler.Level);
		bodies[2].count = PlanetsInLevel(GameRuler.Level);
		bodies[3].count = EarthsInLevel(GameRuler.Level);
		PlayMusic();
	}

	private void Update()
	{
		if (UnityEngine.EventSystems.EventSystem.current.IsPointerOverGameObject())
			return;
		if (bodyIndex >= bodies.Length)
		{
			infoText.text = "";
			GravitySimulator.StartSimulation();
			return;
		}
		if (!GameRuler.CheatsOn)
		{
			if (bodies[bodyIndex].count <= 0)
			{
				bodyIndex++;
				PlayMusic();
				return;
			}
		}
		if (GameRuler.CheatsOn)
		{
			infoText.text = "Countless " + bodies[bodyIndex].prefab.name + "s (Space to continue)";
		}
		else
		{
			infoText.text = bodies[bodyIndex].count > 1
				? bodies[bodyIndex].count + " " + bodies[bodyIndex].prefab.name + "s"
				: "1 " + bodies[bodyIndex].prefab.name;
		}

		if (Input.GetMouseButtonDown(0))
		{
			var mousePos = Input.mousePosition;
			var worldPos = Camera.main.ScreenToWorldPoint(mousePos);
			worldPos.z = 0;
			Instantiate(bodies[bodyIndex].prefab, worldPos, Quaternion.identity);
			if (!GameRuler.CheatsOn)
				bodies[bodyIndex].count--;
		}
		/*else if (GameRuler.CheatsOn && (Input.GetKeyDown(KeyCode.Plus) || Input.GetKeyDown(KeyCode.KeypadPlus)))
		{
			bodies[bodyIndex].count++;
		}*/
		else if (GameRuler.CheatsOn && Input.GetKeyDown(KeyCode.Space))
		{
			bodyIndex++;
			PlayMusic();
		}

	}

	void PlayMusic()
	{
		int timeSamples = 0;
		if (bgAudio.clip != null)
			timeSamples = bgAudio.timeSamples;
		if (bodyIndex < bodies.Length)
			bgAudio.clip = bodies[bodyIndex].audio;
		else
			bgAudio.clip = mainMusic;
		bgAudio.Stop();
		bgAudio.Play();
		bgAudio.timeSamples = timeSamples;
	}

	public static int PlanetsInLevel(int level)
	{
		return (int)(1.2f * level) + 1;
	}
	public static int EarthsInLevel(int level)
	{
		return 1;
		//return 1 + (level / 6);
	}
	public static int BlackholesInLevel(int level)
	{
		return level % 5 == 2 ? (1 + level / 15) : (level / 15);
	}
	public static int StarsInLevel(int level)
	{
		return level % 4 == 1 ? (1 + level / 2) : (level / 2);
	}
}
