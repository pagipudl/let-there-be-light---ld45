﻿Shader "GravityEffectShader"
{
	Properties
	{
		_FallofPower("Falloff", Float) = -5
		_Randomness("Randomness", Float) = 0.2
	}
		SubShader
	{
		Tags {"RenderType" = "Transparent" "Queue" = "Transparent" "LightMode" = "MyCustomPass" }
		Blend One Zero
		BlendOp Add, Add
		Cull Off
		ZWrite Off
		ZTest LEqual

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag


			#include "UnityCG.cginc"
			#include "Assets/Graphics/Noise/HLSL/SimplexNoise2D.hlsl"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
				float4 objPos : TEXCOORD1;
				float4 screenPos : TEXCOOR2;
			};

			sampler2D _MyGrabTexture;
			float4 _MyGrabTexture_TexelSize;
			float _FallofPower;
			float _Randomness;

			float rand(float3 co)
			{
				return frac(sin(dot(co.xyz, float3(12.9898, 78.233, 45.5432))) * 43758.5453);
			}

			v2f vert(appdata v)
			{
				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = v.uv;
				o.objPos = v.vertex;
				o.screenPos = ComputeGrabScreenPos(o.vertex);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
				// sample the texture
				float2 offset = _MyGrabTexture_TexelSize.xy * (1.0 / (pow(1 - length(i.objPos.xy), _FallofPower)))
				* fixed2(_Randomness * snoise(i.screenPos.xy) * sin(10 * _Time.y),
					_Randomness * snoise(i.screenPos.yx) * 8 * cos(_Time.x));
				fixed4 col = tex2D(_MyGrabTexture, i.screenPos.xy / i.screenPos.w + offset);
				//col.g = (1.0 / (pow(1-length(i.objPos.xy), _FallofPower)));
				//col.a = 1;
				return col;
		}
		ENDCG
	}
	}
}
